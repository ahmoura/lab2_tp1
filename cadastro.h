
#ifndef __cadastro__
#define __cadastro__

#include <string>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

class Pessoa {
public:

	string getNome();
	void setNome(string nome);
	string getSobrenome();
	void setSobrenome(string sobrenome);
	int getIdPessoa();
	void setIdPessoa(int idPessoa);
	int getregistro();
	void setregistro(int registro);
	string getCPF();
	void setCPF(string cpf);

private:

	string nome;
	string sobrenome;
	int idPessoa;
	string cpf;
	int registro;
	string path_photo_files;

};

class disciplina : public Pessoa{
public:

	string getProfessor();
	void setProfessor(string Professor);
	int getCoddisciplina();
	void setCoddisciplina(int coddisciplina);
	void setNomedisciplina(string nomedisciplina);
	string getNomedisciplina();

private:
	int coddisciplina;
	string nomedisciplina;
	string Professor;
};

class linf : public disciplina {
public :

	void setProposito(string proposito);
	string getProposito();
	void setHoraAula(int horaAula);
	int getHoraAula();
	int getSala();
	void setSala(int sala);
	void setAteQualDia(string AteQualDia);
	string getAteQualDia();
    void setdiasAula(int diasAula);
    int getdiasAula();

private:
	string proposito;
 	int diasAula;
 	int horaAula;
 	int sala;
 	string AteQualDia;
};

class BancoDados:public linf {
public:
	typedef vector<Pessoa> pessoas_t;
  	pessoas_t vet_pessoas;

  	typedef vector<linf> linf_t;
  	linf_t vetReservas;

  	typedef vector<disciplina> disciplinas_t;
  	disciplinas_t vet_Disciplinas;
    
    
  	void VerificaDisciplinas();
	void AgendaLinf();
	void AddReserva();
	void ResetReservas();
	void AddDisciplinas();
	void ResetDisciplinas();
	void VerificaPessoas();
	void AltDisciplinas();
	void AddUser();
	void AddReservaProf();

private:
    
};

#endif

//************************************************************************************************

