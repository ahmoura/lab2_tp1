#include "cadastro.h"

void MkError();

int main(){
	int confirma ;

	BancoDados bd;

	do{
		system("clear");
        cout << endl;
		cout << "*****************************************************************" << endl;
		cout << "*******************         LINF         ************************" << endl;
		cout << "*****************************************************************" << endl;
		cout << " 1 - Cadastro de alunos e funcionarios                          *" << endl;
		cout << " 2 - Verificar alunos e funcionarios                            *" << endl;
        cout << "-----------------------------------------------------------------" << endl;
		cout << " 3 - Cadastrar disciplinas e professores                        *" << endl;
        cout << " 4 - Deletar disciplinas e professores                          *" << endl;
        cout << " 5 - Alterar disciplinas e professores                          *" << endl;
        cout << " 6 - Verificar disciplinas e professores                        *" << endl;
        cout << "-----------------------------------------------------------------" << endl;
        cout << " 7 - Adicionar reservas professores                             *" << endl;
        cout << " 8 - Adicionar reserva para alunos e outros                     *" << endl;
        cout << " 9 - Retirar reserva                                            *" << endl;
        cout << "10 - Agenda linf                                                *" << endl;
        cout << "-----------------------------------------------------------------" << endl;
		cout << "11 - Sair                                                       *" << endl;
		cout << "*****************************************************************" << endl;
		cout << endl;
		cout << "Opcao : ";
		cin >> confirma;

		switch(confirma){
			case 1: bd.AddUser();  break;

			case 2: bd.VerificaPessoas(); break;

			case 3: bd.AddDisciplinas(); break;

			case 4: bd.ResetDisciplinas(); break;

			case 5: bd.AltDisciplinas(); break;

			case 6: bd.VerificaDisciplinas(); break;

			case 7: bd.AddReservaProf(); break;

			case 8: bd.AddReserva(); break;

			case 9: bd.ResetReservas(); break;

			case 10: bd.AgendaLinf(); break;

			case 11: break;
			default: MkError(); break;
		}
	}while(confirma != 11);

	return 0;
}


void MkError(){
	cout<< "Digite uma entrada valida!" <<endl;
	cout<< "Tecle enter para retornar ao menu!" <<endl;
	getchar();
}


