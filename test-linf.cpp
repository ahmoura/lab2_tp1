#include "cadastro.h"

void Reconigze();
void MkTimeGrid();
void RmTimeGrid();
void MkUser();
void AlUser();
void RmUser();


void SetupTimeGrid(){
	
	int timegridop, timegridexit = 0;

	do{
		system("clear");	
		cout<<"Alterar Grade:"<<endl;
		cout<<"1 - Adicionar disciplina"<<endl;
		cout<<"2 - Excluir disciplina"<<endl;
		cout<<"3 - Voltar"<<endl;
		cout<<"Opcao: ";
		cin >> timegridop;

		switch(timegridop){
		case 1: MkTimeGrid(); break;
		case 2: RmTimeGrid(); break;
		case 3: timegridexit = 1; break;
		}

	}while (timegridexit != 1);

}

void SetupUser(){
	
	int userop, userexit = 0, aux;

	do{
		system("clear");
		cout<<"Alterar Usuario:"<<endl;
		cout<<"1 - Adicionar "<<endl;
		cout<<"2 - Editar"<<endl;
		cout<<"3 - Excluir"<<endl;
		cout<<"4 - Voltar"<<endl;
		cout<<"Opcao: ";
		cin >> userop;

		switch(userop){
		case 1: MkUser(); break;
		case 2: AlUser(); break;
		case 3: RmUser(); break;
		case 4: userexit = 1; break;
		}

	}while (userexit != 1);
}

void Setup(){

	int changesop, changeexit = 0;

	do{
		system("clear");		
		cout<<"Alterar:"<<endl;
		cout<<"1 - Usuario "<<endl;
		cout<<"2 - Grade"<<endl;
		cout<<"3 - Voltar"<<endl;
		cout<<"Opcao: ";
		cin >> changesop;

		switch(changesop){
		case 1: SetupUser(); break;
		case 2: SetupTimeGrid(); break;
		case 3: changeexit = 1; break;

		}
	}while (changeexit != 1);
}

wday ChangeDay(){

	int intaux;
	wday dayaux;

	cout<< "Digite um dia:" << endl;
	cin >> intaux;

	dayaux = DayConvert(intaux);

	return dayaux;
}

wtime ChangeTime(){

	int intaux;
	wtime timeaux;

	cout<< "Digite um horario:" << endl;
	cin >> aux;

	timeaux = TimeConvert(intaux);

	return timeaux;
}

void MkError(){
	cout<< "Digite uma entrada valida!" <<endl;
	cout<< "Tecle enter para retornar ao menu!" <<endl;
	getchar();
}

int main(){

	int choice = 0, exit = 0;
	wday day = ChangeDay();
	wtime time = ChangeTime();


	do{
		system("clear");

		cout<<"LINF:"<<endl;
		cout<<"1 - Acesso "<<endl;
		cout<<"2 - Alterar"<<endl;
		cout<<"3 - Mudar Hora:"<<endl;
		cout<<"4 - Mudar Dia:"<<endl;
		cout<<"5 - Sair"<<endl;
		scanf("%d", &choice);

		switch(choice){
			case 1: Reconigze(); break;
			case 2: Setup(); break;
			case 3: time = ChangeTime(); break;
			case 4: day = ChangeDay(); break;
			case 5: exit = 1; break;
			default: MkError(); break;

		}

	}while(exit != 1);



	return 0;
}

wtime TimeConvert(int var_time){

	switch (var_time){
		case 06: return H06; break;
		case 08: return H08; break;
		case 10: return H10; break;
		case 12: return H12; break;
		case 14: return H14; break;
		case 16: return H16; break;
		case 18: return H18; break;
		case 20: return H20; break;
		case 22: return H22; break;
		default: return TIME_ERROR; break;
	}

}

wday DayConvert(int var_day){

	switch (var_day){
		case 1: return SEG; break;
		case 2: return TER; break;
		case 3: return QUA; break;
		case 4: return QUI; break;
		case 5: return SEX; break;
		case 6: return SAB; break;
		case 7: return DOM; break;
		default: return DAY_ERROR; break;
	}

}

void Convert(int *auxday, int *auxtime, wtime var_time,  wday var_day){

		switch (var_time){
			case H06: *auxtime = 0; break;
			case H08: *auxtime = 1; break;
			case H10: *auxtime = 2; break;
			case H12: *auxtime = 3; break;
			case H14: *auxtime = 4; break;
			case H16: *auxtime = 5; break;
			case H18: *auxtime = 6; break;
			case H20: *auxtime = 7; break;
			case H22: *auxtime = 8; break;
		}	

		switch (var_day){
			case SEG: *auxday = 0; break;
			case TER: *auxday = 1; break;
			case QUA: *auxday = 2; break;
			case QUI: *auxday = 3; break;
			case SEX: *auxday = 4; break;
			case SAB: *auxday = 5; break;
			case DOM: *auxday = 6; break;
		}
}