#include <GL/glut.h>
#include <string.h>


int init(void)
{

char **amp;
void* draw;


//general initialization:
	char cadena[25]="GestionMarcaAR\0";
	char **argv;
	int argc = 1;
 
	argv=(char **)malloc(sizeof(char *));
	*argv=(char *)malloc(sizeof(char)*10);
	strcpy(*argv, cadena);
 
	glutInit(&amp,argc,argv); //we cheat it ;P
 
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(800, 400);
	glutInitWindowSize(320,240);
	glutCreateWindow("Name");
 
	glutDisplayFunc(draw);
	glutIdleFunc(draw);
 
//initializating the texture mapping
	GLuint mTexture;
	glGenTextures(1, &amp,mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);
	glEnable(GL_TEXTURE_2D);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
 
 
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
 
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f); //color blanco de fondo
	glColor3f(0.0f,0.0f,0.0f);
 
	return 0;
}

void draw()
{

void *imagem;

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,320,240,0,GL_BGR,GL_UNSIGNED_BYTE, imagem->imageData);
 
	glBegin(GL_POLYGON);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(-1.0f, -1.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f( 1.0f, -1.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f( 1.0f,  1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(-1.0f,  1.0f);
	glEnd();
 
	glFlush();
 
	glutSwapBuffers();
}


int main(){

	glutPostRedisplay();
	glutMainLoopEvent();   //freeglut function	

	return 0;

}
